#include<bits/stdc++.h>
using namespace std;

#define ull unsigned long long
#define MAX 10000

ull ugly[MAX];

void uglyNumbers() {
    ull next_multiple_of_2 = 2;
    ull next_multiple_of_3 = 3;
    ull next_multiple_of_5 = 5;
    int i2 = 0, i3 = 0, i5 = 0;
    ugly[0] = 1;
    for(int i=1; i<MAX; i++) {
        ugly[i] = min(next_multiple_of_2, min(next_multiple_of_3, next_multiple_of_5));
        if(ugly[i] == next_multiple_of_2) {
            i2 = i2+1;
            next_multiple_of_2 = ugly[i2]*2;
        }
        if(ugly[i] == next_multiple_of_3) {
            i3 = i3+1;
            next_multiple_of_3 = ugly[i3]*3;
        }
        if(ugly[i] == next_multiple_of_5) {
            i5 = i5+1;
            next_multiple_of_5 = ugly[i5]*5;
        }
    }
}

int main() {
    int t;
    cin >> t;
    uglyNumbers();
    while(t--) {
        int n;
        cin >> n;
        cout << ugly[n-1] << '\n';
    }
    return 0;
}