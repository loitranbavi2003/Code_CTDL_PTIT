#include<bits/stdc++.h>
using namespace  std;
int N;
double khoangCach(int x1,int y1,int x2,int y2){
    int x = x1-x2;
    int y = y1-y2;
    return sqrt(x*x+y*y);
}
double diemGanNhat(int N,int a[][2]){
	double minimum = 999999999;
	for(int i=0;i<N-1;i++){
		for(int j=i+1;j<N;j++){
			int x1 = a[i][0];
			int y1 = a[i][1];
			int x2 = a[j][0];
			int y2 = a[j][1];
			double d = khoangCach(x1,y1,x2,y2);
			minimum = min(d,minimum);
		}
	}
	return minimum;
}
int main(){
	int t;cin>>t;
	while(t--){
		cin>>N;
		int a[N][2];
		for(int i=0;i<N;i++){
			cin>>a[i][0]>>a[i][1];
		}
		cout<<fixed<<setprecision(6)<<diemGanNhat(N,a)<<endl;
	}
}