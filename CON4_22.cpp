#include<bits/stdc++.h>
using namespace std;

int main(){
	int t;cin>>t;
	while(t--){
		int N,K;
		cin>>N>>K;
		int a[N];
		int check =0;
		int index=-1;
		for(int i=0;i<N;i++){
			cin>>a[i];
			if(a[i]==K&&check==0){
				check = 1;
				index = i;
			}
		}
		if(index == -1) cout<<"NO"<<endl;
		else cout<<index+1<<endl;
	}
}