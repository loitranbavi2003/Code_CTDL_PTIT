#include <bits/stdc++.h>

void HoanViKeTiep(std::vector<int> &arr, int length);

int main()
{
    int test;
    std::cin >> test;
    
    while(test--)
    {
        int n;
        std::vector<int> arr;

        std::cin >> n;
        for(int i = 0; i < n; i++)
        {
            int a;
            std::cin >> a;
            arr.push_back(a);
        }

        HoanViKeTiep(arr, n);
        
        for(int i = 0; i < n; i++)
        {
            std::cout << arr[i];
            if(i < n - 1)
            {
                std::cout << " ";
            }
        }
        std::cout << std::endl;
    }
}

void HoanViKeTiep(std::vector<int> &arr, int length)
{
    int i = length - 2;

    /* tìm vị trí đầu tiên từ phải qua trái mà arr[i] < arr[i+1] */
    while(i >=0 && arr[i] >= arr[i+1])
    {
        i--;
    }
    // std::cout << i << std::endl;
    if(i < 0)
    {
        for(int t = 0; t < length-1; t++)
        {
            for(int m = t+1; m < length; m++)
            {
                std::swap(arr[t], arr[m]);
            }
        }
    }

    /* tìm vị trí đầu tiên từ phải qua trái mà arr[i] < arr[j] */
    int j = length -1;
    while(arr[i] > arr[j])
    {
        j--;
    }
    // std::cout << j << std::endl;
    /* Hoán đổi giá trị của 2 vị trí i và j */
    std::swap(arr[i], arr[j]);

    /* Sắp xếp tăng dần từ vị trí i+1 đến vị trí n-1 */
    for(int t = i+1; t < length-1; t++)
    {
        for(int m = t+1; m < length; m++)
        {
            if(arr[t] > arr[m])
            {
                std::swap(arr[t], arr[m]);
            }
        }
    }
}