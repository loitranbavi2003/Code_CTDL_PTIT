#include<bits/stdc++.h>
using namespace std;
int N;
vector<int> v;
void input(){
    cin>>N;
    for(int i=0;i<N;i++){
        int x; cin>>x;
        v.push_back(x);
    }
}
void output(){
    cout<<"[";
    for(int i=0;i<v.size();i++){
        if(i!=v.size()-1) cout<<v[i]<<" ";
        else cout<<v[i]<<"]";
    }
    cout<<endl;
}
void solve(){
	int k =N-1;
	cout<<"[";
    for(int i=0;i<v.size();i++){
        if(i!=v.size()-1) cout<<v[i]<<" ";
        else cout<<v[i]<<"]";
    }
    cout<<endl;
    while(k!=0){
    	cout<<"[";
        for(int i=0;i<k;i++){
            v[i] = v[i] + v[i+1];
            if(i!=k-1)
            cout<<v[i]<<" ";
            else cout<<v[i]<<"]";
            
        }
        k--;
        cout<<endl;
        //output();
    }
}
int main(){
    int t;cin>>t;
    while(t--){
        input();
        solve();
        v.clear();
    }
}