#include <bits/stdc++.h>

using namespace std;

int n, arr[20], check[20] = {0};

void Print(void);
void Try(int k);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        cin >> n;

        Try(1);
        cout << endl;
    }
}

void Try(int k)
{
    for(int i = 1; i <= n; i++)
    {
        if(check[i] == 0)
        {
            arr[k] = i;     // luu 1 ptu vao hoan vi
            check[i] = 1;   // danh dau da dung
            if(k == n)
            {
                Print();
            }
            else
            {
                Try(k + 1);
            }
            check[i] = 0;
        }
    }
}

void Print(void)
{
    for(int i = 1; i <= n; i++)
    {
        cout << arr[i];
    }
    cout << " ";
}