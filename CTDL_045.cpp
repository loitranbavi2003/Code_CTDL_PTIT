#include<bits/stdc++.h>
using namespace std;
bool ctdl045(string s){
    int a[27];
    for(int i=0;i<27;i++) a[i] = 0;
    for(int i=0;i<s.size();i++){
        int x = s[i]-'a';
        a[x]++;
    }
    for(int i=0;i<27;i++){
        if(a[i]>(s.size()+1)/2) return false;
    }
    return true;
}
int main(){
    int t;cin>>t;
    while(t--){
        string s;cin>>s;
        if(ctdl045(s)) cout<<1<<endl;
        else cout<<-1<<endl;
    }
        
}