#include<bits/stdc++.h>
using namespace std;
#define MOD 1000000007
int toHop(int N,int K){
    if(K==0||K==N) return 1;
    int dp[N+1][K+1];
    for(int i=0;i<=N;i++){
        for(int j=0;j<=min(i,K);j++){
            if(j==0||j==i)  dp[i][j] =1;
            else dp[i][j] = (dp[i-1][j-1]+dp[i-1][j])%MOD;
		}
    }
    return dp[N][K];
}
int main(){
	int t;cin>>t;
    while(t--){
        int N,K;
        cin>>N>>K;
        if(N<K) cout<<0<<endl;
        else
        cout<<toHop(N,K)<<endl;
    }
}