#include <bits/stdc++.h>

using namespace std;

int n, arr[20], check[20] = {0};

void Print(void);
void Try(int k);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        cin >> n;

        Try(1);
        cout << endl;
    }
}

void Try(int k)
{
    for(int i = n; i >= 1; i--)
    {
        if(check[i] == 0)
        {
            check[i] = 1;
            arr[k] = i;

            if(k == n)
            {
                Print();
            }
            else
            {
                Try(k+1);
            }

            check[i] = 0;
        }
    }
}

void Print(void)
{
    for(int i = 1; i <= n; i++)
    {
        cout << arr[i];
    }
    cout << " ";
}