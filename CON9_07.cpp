#include <iostream>
#include <vector>
#include <unordered_set>
#include <stack>
using namespace std;

void dfs(vector<unordered_set<int>>& graph, int start, vector<bool>& visited, vector<int>& result) {
    stack<int> s;
    s.push(start);

    while (!s.empty()) {
        int vertex = s.top();
        s.pop();

        if (!visited[vertex]) {
            visited[vertex] = true;
            result.push_back(vertex);

            for (auto neighbor : graph[vertex]) {
                if (!visited[neighbor]) {
                    s.push(neighbor);
                }
            }
        }
    }
}

int main() {
    int T;
    cin >> T;

    while (T--) {
        int V, E, u;
        cin >> V >> E >> u;

        vector<unordered_set<int>> graph(V + 1);
        vector<bool> visited(V + 1, false);
        vector<int> result;

        for (int i = 0; i < E; ++i) {
            int u, v;
            cin >> u >> v;
            graph[u].insert(v);
        }

        dfs(graph, u, visited, result);

        for (auto vertex : result) {
            cout << vertex << " ";
        }
        cout << endl;
    }

    return 0;
}