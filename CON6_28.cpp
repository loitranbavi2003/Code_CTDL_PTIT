#include<bits/stdc++.h>
using namespace std;
int main(){
	int t;cin>>t;
    while(t--){
        int N;
        cin>>N;
        int a[N+1];
        for(int i=1;i<=N;i++) cin>>a[i];
        vector< vector<int> > v;
        vector<int> x;
        for(int i=1;i<=N;i++){
            x.push_back(a[i]);
            sort(x.begin(),x.end());
            v.push_back(x);
        }
        for(int i=v.size()-1;i>=0;i--){
        	cout<<"Buoc "<<i<<": ";
        	for(int j=0;j<v[i].size();j++) cout<<v[i][j]<<" ";
        	cout<<endl;
		}
    }
}