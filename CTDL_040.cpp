#include<bits/stdc++.h>
using namespace std;
int N;
int a[30];
long long tongNhoNhat(int N,int a[]){
    sort(a,a+N);
    long long so1 = 0,so2 = 0;
    for(int i=0;i<N;i++){
        if(i%2==0) so1 = so1*10 + a[i];
        else so2 = so2*10 + a[i];
    }
    return so1+so2;
}
int main(){
	int t;cin>>t;
    while(t--){
        cin>>N;
        for(int i=0;i<N;i++) cin>>a[i];
        cout<<tongNhoNhat(N,a)<<endl;
    }
}