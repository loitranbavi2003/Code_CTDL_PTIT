#include <bits/stdc++.h>

using namespace std;

void Chuoi_Nhi_Phan_HopLe(string &str, int i, vector<string> &results);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        string str;
        cin >> str;

        vector<string> results;
        Chuoi_Nhi_Phan_HopLe(str, 0, results);

        for(const auto& str : results)
        {
            cout << str << endl;
        }
    }
}

void Chuoi_Nhi_Phan_HopLe(string &str, int i, vector<string> &results)
{
    if(i == str.length())
    {
        results.push_back(str);
        return;
    }

    if(str[i] == '?')
    {
        str[i] = '0';
        Chuoi_Nhi_Phan_HopLe(str, i + 1, results);
        str[i] = '1';
        Chuoi_Nhi_Phan_HopLe(str, i + 1, results);
        str[i] = '?'; // Reset for backstracking
    }
    else
    {
        Chuoi_Nhi_Phan_HopLe(str, i + 1, results);
    }
}

/*
000
001
3 = ?

*/