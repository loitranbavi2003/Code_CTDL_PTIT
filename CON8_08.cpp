#include<bits/stdc++.h>
using namespace std;
int con8_07(int N){
    queue<int> q;
    q.push(1);
    while(q.front()%N!=0&&!q.empty()){
        int x = q.front();
            q.push(x*10);
            q.push(x*10+1);
        q.pop();
    }
    return q.front();
}
int main(){
    int t;cin>>t;
    while(t--){
        int N;cin>>N;
        cout<<con8_07(N)<<endl;
	}
}