#include<bits/stdc++.h>
using namespace std;

int maxSumIS(int A[], int N) {
    int dp[N];
    for (int i=0; i<N; i++)
        dp[i] = A[i];

    for (int i=1; i<N; i++)
        for (int j=0; j<i; j++)
            if (A[i] > A[j] && dp[i] < dp[j] + A[i])
                dp[i] = dp[j] + A[i];

    return *max_element(dp, dp+N);
}

int main() {
    int T;
    cin >> T;
    while(T--) {
        int N;
        cin >> N;
        int A[N];
        for(int i=0; i<N; i++)
            cin >> A[i];
        cout << maxSumIS(A, N) << '\n';
    }
    return 0;
}