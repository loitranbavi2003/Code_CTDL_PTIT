#include<bits/stdc++.h>
using namespace std;
int tinhGiaTri(int a,int b,char c){
    if(c=='+') return a+b;
    if(c=='-') return a-b;
    if(c=='*') return a*b;
    if(c=='/') return a/b;
    if(c=='%') return a%b;
    if(c=='^') return pow(a,b);   
}
int con7_16(string s){
    int tong = 0;
    stack<int> st;
    for(int i=0;i<s.size();i++){
        if(s[i]>='0'&&s[i]<='9') st.push(s[i]-'0');
        else{
            int x = st.top();st.pop();
            int y = st.top();st.pop();
            int z = tinhGiaTri(y,x,s[i]);
            st.push(z);
        }
    }
    int x = st.top();
    return x;
}
int main(){
    int t;cin>>t;
    while(t--){
        string s;cin>>s;
        cout<<con7_16(s)<<endl;
    }
}