#include<bits/stdc++.h>
using namespace std;
#define MOD 123456789
long long mul(long long a,long long b){
    return ((a%MOD)*(b%MOD))%MOD;
}
long long power(long long N,long long a){
    if(N==0) return 1;
    if(N==1) return a;
    if(N%2==0){
        long long half_power = power(N/2,a);
        return mul(half_power,half_power);
    }
    else{
        long long half_power = power(N/2,a);
        long long powerr = mul(half_power,half_power);
        return mul(powerr,a);
    }
}
int main(){
    int t;cin>>t;
    while(t--){
        long long N;cin>>N;
        cout<<power(N-1,2)<<endl;
	}
}