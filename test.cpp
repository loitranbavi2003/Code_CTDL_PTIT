#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll, ll> pll;

#define x first
#define y second

double dist(pll a, pll b) {
    return sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
}

double closest(vector<pll>& points, int n) {
    double min_val = DBL_MAX;
    for (int i = 0; i < n; ++i)
        for (int j = i+1; j < n; ++j)
            if (dist(points[i], points[j]) < min_val)
                min_val = dist(points[i], points[j]);
    return min_val;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int T;
    cin >> T;
    while (T--) {
        int N;
        cin >> N;
        vector<pll> points(N);
        for (int i = 0; i < N; i++)
            cin >> points[i].x >> points[i].y;
        printf("%.6f\n", closest(points, N));
    }
    return 0;
}