#include<bits/stdc++.h>
using namespace std;
int con5_10(vector< vector<int> >&a, int N,int M){
    for(int i = 2;i<=M;i++) a[1][i]+= a[1][i-1];
    for(int i=2;i<=N;i++) a[i][1]+= a[i-1][1];
    for(int i = 2;i<=N;i++){
        for(int j = 2;j <= M;j++){
            a[i][j]+= min(a[i-1][j-1],min(a[i-1][j],a[i][j-1]));
        }
    }
    return a[N][M];
}
int main(){
    int t;cin>>t;
    while(t--){
        int N,M;
        cin>>N>>M;
        vector< vector<int> > a(N+1,vector<int>(M+1));
        for(int i=1;i<=N;i++){
            for(int j = 1;j<=M;j++) cin>>a[i][j];
        }
        cout<<con5_10(a,N,M)<<endl;
    }
}