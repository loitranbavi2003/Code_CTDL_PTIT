#include<bits/stdc++.h>
using namespace std;
long long timMax(vector<int> v,int N){
    sort(v.begin(),v.end());
    long long tong = 0;
    for(int i=0;i<N;i++){
        tong = tong + i*v[i];
    }
    return tong;
}
int main(){
	int t;cin>>t;
    while(t--){
        int N;
        cin>>N;
        vector<int> v(N);
        for(int i=0;i<N;i++){
		 cin>>v[i];}
        cout<<timMax(v,N)<<endl;
    }
}