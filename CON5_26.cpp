#include <iostream>
#include <algorithm>
using namespace std;
int main (){
    int t;
    cin >> t;
    while(t--){
        int c,n;
        cin >> c >> n;
        int w[n];
        for(int i = 0;i < n;i++) cin >> w[i];
        sort(w,w+n);
        int sum = 0,i = n-1;
        while(c >= w[i] && i >= 0){
            c -= w[i];
            sum += w[i];
            i--;
        }
        cout << sum << endl;
    }
    return 0;
}