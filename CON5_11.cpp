#include<bits/stdc++.h>
using namespace std;
long long catalan(long long a[],int N){
    for(int i=1;i<=N;i++){ //1 1 2 0 0
        for(int j=0;j<=i-1;j++){
            a[i] += a[j]*a[i-j-1];
        }
    }
    return a[N-1];
}
int main(){
    int t;cin>>t;
    while(t--){
        int N;
        cin>>N;
        long long a[N+1];
       for(int i=0;i<=N;i++) a[i] = 0;
        a[0] = 1;
        cout<<catalan(a,N+1)<<endl;
	}
}