#include<bits/stdc++.h>
using namespace std;
int N,K;
#define MOD 1000000007
	long long dp[105][50005] = {};
int main(){
	for(int i=1;i<=9;i++) dp[1][i] = 1;
	for(int i=2;i<=100;i++){
		for(int j = 901;j>=0;j--){
			for(int k = 0;k<=9;k++){
				if(j>=k){
					dp[i][j]+=dp[i-1][j-k];
					dp[i][j]%=MOD;
				}
			}
		}
	}
    int t;cin>>t;
    while(t--){
        cin>>N>>K;
        cout<<dp[N][K]<<endl;
    }
}