#include<bits/stdc++.h>
using namespace std;
int main(){
	int t;cin>>t;
    while(t--){
        long long N,X;
        cin>>N>>X;
        long long A[N];
        long long index = -1;
        //long long value = 0;
        for(long long i=0;i<N;i++){
            cin>>A[i];
        }
        sort(A,A+N);
        for(long long i=0;i<N;i++){
            if(A[i]<=X) index = i+1;
        }
        cout<<index<<endl;
    }
}