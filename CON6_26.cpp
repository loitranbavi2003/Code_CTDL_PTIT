#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;cin>>t;
    while(t--){
        int N;
        cin>>N;
        int A[N];
        for(int i=1;i<=N;i++) cin>>A[i];
        for(int i=1;i<=N-1;i++){
            for(int j=i;j<=N;j++){
                if(A[i]>A[j]){
                    int swap = A[i];
                    A[i] = A[j];
                    A[j] = swap;
                }
            }
            cout<<"Buoc "<<i<<": ";
                for(int i=1;i<=N;i++) cout<<A[i]<<" ";
                cout<<endl;
        }
    }
}