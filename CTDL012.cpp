#include <bits/stdc++.h>

using namespace std;

long long BoiSo(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        long long result = BoiSo(n);
        cout << result << endl;
    }
}

long long BoiSo(int n)
{
    if(n == 0)
    {
        return 0;
    }
    else if(n == 1 || n == 3 || n == 9)
    {
        return 9;
    }

    queue<string> q;
    q.push("9");
    while(1)
    {
        string str = q.front();
        q.pop();
        long long x = stoll(str + "0");
        if(x%n == 0)
        {
            return x;
        }
        else
        {
            q.push(str + "0");
            long long y = stoll(str + "9");
            if(y%n == 0)
            {
                return y;
            }
            else
            {
                q.push(str + "9");
            }
        }
        // cout << q.front() << endl;
    }
}