#include<bits/stdc++.h>
using namespace std;
int con8_07(int N){
    if(N==0) return 1;
    int cnt = 1;
    queue<int> q;
    q.push(1);
    while(q.front()<=N&&!q.empty()){
        int x = q.front();
        if(x*10<=N){
            q.push(x*10);
            cnt++;
        }
        if(x*10+1<=N){
            q.push(x*10+1);
            cnt++;
        }
        q.pop();
    }
    return cnt;
}
int main(){
    int t;cin>>t;
    while(t--){
        int N;cin>>N;
        cout<<con8_07(N)<<endl;
	}
}