#include<bits/stdc++.h>
using namespace std;
int main(){
    int t;cin>>t;
    while(t--){
        int N;cin>>N;
    	bool a[N+1];
   	 	memset(a,true,sizeof(a));
    	a[0] = a[1] = false;
    	for(int i=2;i<=N;i++){
       	 	if(a[i]==true){
            	for(int j=i+i;j<=N;j+=i) a[j] = false;
        	}
    	}
        int ok =0;
        int i;
        for( i=2;i<=N;i++){
            if(a[i]==true&&a[N-i]==true){
                ok=1;
                break;
            }
        }
        if(ok==0) cout<<-1<<endl;
        else cout<<i<<" "<<N-i<<endl;
    }
}