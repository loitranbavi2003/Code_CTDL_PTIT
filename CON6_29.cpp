#include<bits/stdc++.h>
using namespace std;
int main(){
	int t;cin>>t;
    while(t--){
        int N;
        cin>>N;
        int a[N];
        for(int i=0;i<N;i++) cin>>a[i];
        int check = 0;
        int dem = 1;
        while(1){
        	for(int i=0;i<N-1;i++){
        		if(a[i]>a[i+1]){
        			int swap = a[i];
        			a[i] = a[i+1];
        			a[i+1] = swap;
        			check = 1;
				}
			}
			if(check==0) break;
			else{
				cout<<"Buoc "<<dem<<": ";
				for(int i=0;i<N;i++){
					cout<<a[i]<<" ";
				}
				cout<<endl;
			}
			check = 0;
			dem++;
		}
    }
}