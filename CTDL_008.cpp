#include<bits/stdc++.h>
using namespace std;
string maGray(string s){
	string result = "";
    result+=s[0];
    for(int i=1;i<s.size();i++){
        if(s[i]==s[i-1]) result+="0";
        else result+="1";
    }
    return result;
}
int main(){
	int t;
    cin>>t;
    while(t--){
        string s;
        cin>>s;
        cout<<maGray(s)<<endl;
    }
}