#include <bits/stdc++.h>

using namespace std;

string String_Next(string &str);

int main()
{
    int test;
    string str;
    cin >> test;
    cin.ignore();
    while(test--)
    {
        cin >> str;

        string result = String_Next(str);
        cout << result << endl;
    }
}

string String_Next(string &str)
{
    int l = str.size();

    while(l-- && str[l] == '1')
    {
        str[l] = '0';
    }

    if(l >= 0)
    {
        str[l] = '1';
    }

    return str;
}