#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        int count = 0;
        int arr_tien[10] = {1000, 500, 200, 100, 50, 20, 10, 5, 2, 1};

        for(int i = 0; i < 10; i++)
        {
            if(n >= arr_tien[i])
            {
                count += n/arr_tien[i];
                n = n%arr_tien[i];
            }
            else if(n == 0)
            {
                break;
            }
        }

        cout << count << endl;
    }
}