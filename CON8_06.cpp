#include<bits/stdc++.h>
using namespace std;

string findSmallest(int N) {
    queue<string> q;
    set<int> visit;

    string t = "9";

    q.push(t);

    while (!q.empty()) {
        t = q.front(); q.pop();

        int rem = 0;
        for (int i = 0; i < t.length(); i++)
            rem = (rem*10 + (t[i]-'0')) % N;

        if (rem == 0)
            return t;

        if(visit.find(rem) == visit.end()) {
            visit.insert(rem);
            q.push(t + "0");
            q.push(t + "9");
        }
    }
    return "";
}

int main() {
    int T;
    cin >> T;
    while (T--) {
        int N;
        cin >> N;
        cout << findSmallest(N) << endl;
    }
    return 0;
}