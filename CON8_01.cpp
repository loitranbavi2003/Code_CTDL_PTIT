#include<bits/stdc++.h>
using namespace std;
int main(){
	int t;cin>>t;
	while(t--){
		int N;cin>>N;
		queue<string> q;
		vector<string> v;
		string k;
		for(int i=1;i<=N;i++){
			int x;cin>>x;
			if(x==3){
				string y;cin>>y;
				q.push(y);
				k = y;
			}
			else if(x==5){
				if(q.empty()) v.push_back("-1");
				else v.push_back(q.front());
			}
			else if(x==1){
				 int z = q.size();
				 string a = to_string(z);
				v.push_back(a);
			}
			else if(x==4){
				if(!q.empty()) q.pop();
			}
			else if(x==6){
             if(!q.empty()) v.push_back(k);
                else v.push_back("-1");
            }
			else{
				if(q.empty()) v.push_back("YES");
				else v.push_back("NO");
			}
		}
		for(int i=0;i<v.size();i++){
			cout<<v[i]<<endl;
		}
	}
}