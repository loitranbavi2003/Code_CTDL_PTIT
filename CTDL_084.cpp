#include<bits/stdc++.h>
using namespace std;
int solve(int n){
    queue<int> q;
    q.push(9);
    while(q.front()%n!=0){
        int x = q.front();
        q.pop();
        q.push(x*10);
        q.push(x*10+9);
    }
    return q.front();
}
int main(){
	int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        cout<<solve(n)<<endl;
	}
}