#include<bits/stdc++.h>
using namespace std;
string prefix_to_infix(string s){
    stack<string> st;
    for(int i=s.size()-1;i>=0;i--){
        if(s[i]>='a'&&s[i]<='z') st.push(string(1,s[i]));
        else{
            string x = st.top();st.pop();
            string y = st.top();st.pop();
            string z = "(" + x + s[i] + y + ")";
            st.push(z);
        }
    }
    string x = st.top();
    return x;
}
int main(){
    int t;cin>>t;
    while(t--){
        string s;cin>>s;
        cout<<prefix_to_infix(s)<<endl;
    }
}