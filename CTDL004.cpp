#include <bits/stdc++.h>

using namespace std;

void Generate_String(string str, int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n;
        cin >> n;

        Generate_String("", n);
        cout << endl;
    }
}

void Generate_String(string str, int n)
{
    if(str.length() == n)
    {
        cout << str << " ";
        return;
    }

    Generate_String(str + 'A', n);
    Generate_String(str + 'B', n);
}