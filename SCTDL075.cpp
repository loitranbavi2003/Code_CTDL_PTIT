#include<bits/stdc++.h>
using namespace std;

int minOperations(vector<int>& target) {
    int operations = target[0];
    for(int i = 1; i < target.size(); i++) {
        if(target[i] > target[i-1]) {
            operations += target[i] - target[i-1];
        }
    }
    return operations;
}

int main() {
    int t;
    cin >> t;
    while(t--) {
        int n;
        cin >> n;
        vector<int> target(n);
        for(int i = 0; i < n; i++) {
            cin >> target[i];
        }
        cout << minOperations(target) << endl;
    }
    return 0;
}