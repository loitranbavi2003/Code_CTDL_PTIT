#include <iostream>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
};

void dfs(TreeNode* node, vector<int>& path, vector<vector<int>>& paths) {
    if (node == nullptr)
        return;

    path.push_back(node->val);

    if (node->left == nullptr && node->right == nullptr) {
        paths.push_back(path);
    }

    dfs(node->left, path, paths);
    dfs(node->right, path, paths);

    path.pop_back();
}

vector<vector<int>> binaryTreePaths(TreeNode* root) {
    vector<vector<int>> paths;
    vector<int> path;
    dfs(root, path, paths);
    return paths;
}

TreeNode* buildTree(vector<int>& nums, int i) {
    if (i >= nums.size() || nums[i] == -1)
        return nullptr;

    TreeNode* root = new TreeNode(nums[i]);
    root->left = buildTree(nums, 2 * i + 1);
    root->right = buildTree(nums, 2 * i + 2);
    return root;
}

void printPaths(vector<vector<int>>& paths) {
    for (const auto& path : paths) {
        for (int i = 0; i < path.size(); i++) {
            cout << path[i];
            if (i < path.size() - 1)
                cout << "->";
        }
        cout << endl;
    }
}

int main() {
    int t;
    cin >> t;

    while (t--) {
        int n;
        cin >> n;
        vector<int> nums(n);
        for (int i = 0; i < n; i++) {
            cin >> nums[i];
        }

        TreeNode* root = buildTree(nums, 0);
        vector<vector<int>> paths = binaryTreePaths(root);
        printPaths(paths);
    }

    return 0;
}