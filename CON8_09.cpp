#include<bits/stdc++.h>
using namespace std;

int con8_09(int S, int T) {
    queue<pair<int, int>> q; // Sử dụng pair để lưu cả số vị trí và số bước đi
    q.push({S, 0}); // S là vị trí ban đầu, số bước đi là 0
    vector<bool> visited(T*2 + 1, false); // Đánh dấu vị trí đã thăm qua
    visited[S] = true;

    while (!q.empty()) {
        int current_pos = q.front().first;
        int steps = q.front().second;
        q.pop();

        if (current_pos == T) // Nếu đến được vị trí T, trả về số bước
            return steps;

        // Thử di chuyển đến các vị trí kế tiếp (current_pos - 1 và current_pos * 2)
        if (current_pos - 1 >= 0 && !visited[current_pos - 1]) {
            q.push({current_pos - 1, steps + 1});
            visited[current_pos - 1] = true;
        }
        if (current_pos * 2 <= T * 2 && !visited[current_pos * 2]) {
            q.push({current_pos * 2, steps + 1});
            visited[current_pos * 2] = true;
        }
    }
    return -1; // Nếu không tìm thấy đường đi từ S đến T
}

int main(){
    int t; cin >> t;
    while(t--){
        int S, T;
        cin >> S >> T;
        cout << con8_09(S, T) << endl;
    }
    return 0;
}