#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct Node {
    int data;
    Node* left;
    Node* right;
};

Node* createNode(int data) {
    Node* newNode = new Node();
    newNode->data = data;
    newNode->left = newNode->right = NULL;
    return newNode;
}

void spiralOrderTraversal(Node* root) {
    if (root == NULL)
        return;

    queue<Node*> q;
    q.push(root);

    bool reverse = false;

    while (!q.empty()) {
        int levelSize = q.size();
        vector<int> levelNodes;

        for (int i = 0; i < levelSize; i++) {
            Node* node = q.front();
            q.pop();

            levelNodes.push_back(node->data);

            if (node->left)
                q.push(node->left);
            if (node->right)
                q.push(node->right);
        }

        if (reverse) {
            for (int i = levelNodes.size() - 1; i >= 0; i--)
                cout << levelNodes[i] << " ";
        } else {
            for (int i = 0; i < levelNodes.size(); i++)
                cout << levelNodes[i] << " ";
        }

        reverse = !reverse;
    }
}

int main() {
    int T;
    cin >> T;

    while (T--) {
        int N;
        cin >> N;

        Node* root = NULL;

        for (int i = 0; i < N; i++) {
            int u, v;
            char x;
            cin >> u >> v >> x;

            if (root == NULL) {
                root = createNode(u);
                if (x == 'L')
                    root->left = createNode(v);
                else if (x == 'R')
                    root->right = createNode(v);
            } else {
                Node* parent = NULL;
                Node* child = NULL;

                queue<Node*> q;
                q.push(root);

                while (!q.empty()) {
                    parent = q.front();
                    q.pop();

                    if (parent->data == u) {
                        if (x == 'L') {
                            parent->left = createNode(v);
                            break;
                        } else if (x == 'R') {
                            parent->right = createNode(v);
                            break;
                        }
                    }

                    if (parent->left)
                        q.push(parent->left);
                    if (parent->right)
                        q.push(parent->right);
                }
            }
        }

        spiralOrderTraversal(root);

        cout << endl;
    }

    return 0;
}