#include <bits/stdc++.h>

using namespace std;

int n;
int arr[100];

void Display(int a[]);
void DeQuy(int i);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        cin >> n;

        DeQuy(1);
    }
}

void Display(int a[])
{
    for(int i = 1; i <= n; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
}

void DeQuy(int i)
{
    for(int j = 0; j <= 1; j++)
    {
        arr[i] = j;

        if(i == n)
        {
            Display(arr);
        }
        else
        {
            DeQuy(i + 1);
        }
    }
}