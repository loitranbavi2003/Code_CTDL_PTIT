#include <bits/stdc++.h>

using namespace std;

int n;
vector<int> arr;

void InPut(void);
void OutPut(void);
void Sinh(void);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {    
        InPut();
        OutPut();
        Sinh();
        arr.clear();
    }
}

void InPut(void)
{
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        int x;
        cin >> x;
        arr.push_back(x);
    }
}

void OutPut(void)
{
    cout << "[";
    for(int i = 0; i < n; i++)
    {
        cout << arr[i];
        if(i != n - 1)
        {
            cout << " ";
        }
    }
    cout << "]" << endl;
}

void Sinh(void)
{
    while(n > 1)
    {
        for(int i = 0; i < n-1; i++)
        {
            arr[i] = arr[i] + arr[i+1];
        }
        n--;
        OutPut();
    }
}