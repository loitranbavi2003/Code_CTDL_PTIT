#include <iostream>
#include <queue>
using namespace std;
int main (){
    string s;
    int n;
    queue<int> st;
    while(cin >> s){
        if(s == "push"){
            cin >> n;
            st.push(n);
        }
        else if(s == "pop"){
            if(!st.empty()) st.pop();
        }
        else if(s == "show"){
            if(st.empty()) cout << "empty";
            else {
                queue<int> temp = st;
                while(!temp.empty()){
                    cout << temp.front() << " ";
                    temp.pop();
                }
            }
            cout << endl;
        }
    }
    return 0;
}