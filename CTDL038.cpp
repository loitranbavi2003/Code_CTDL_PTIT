#include <bits/stdc++.h>

using namespace std;

int Min(int n);
int Max(int n);

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int a, b;
        cin >> a >> b;

        int min_a, min_b;
        int max_a, max_b;

        min_a = Min(a);
        min_b = Min(b);
        max_a = Max(a);
        max_b = Max(b);

        cout << min_a + min_b << " " << max_a + max_b << endl;
    }
}

int Min(int n)
{
    string str = to_string(n);
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '5')
        {
            str[i] = '3';
        }
    }

    int result = stoi(str);
    return result;
}

int Max(int n)
{
    string str = to_string(n);
    for(int i = 0; i < str.length(); i++)
    {
        if(str[i] == '3')
        {
            str[i] = '5';
        }
    }

    int result = stoi(str);
    return result;
}