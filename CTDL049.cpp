#include <bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    cin >> test;
    while(test--)
    {
        int n, k, vtri, flag = 0;
        int arr[100000];

        cin >> n >> k;
        for(int i = 1; i <= n; i++)
        {
            cin >> arr[i];

            if(k == arr[i])
            {
                vtri = i;
                flag = 1;
            }
        }

        if(flag == 1)
        {
            cout << vtri << endl;
        }
        else
        {
            cout << "NO" << endl;
        }
    }
}