#include<bits/stdc++.h>
using namespace std;
int N;

long long gtnn(vector<long long> a,vector<long long> b,int N){
    sort(a.begin(),a.end());
    sort(b.begin(),b.end());
    long long tong = 0;
    for(int i=0;i<N;i++){
        tong = tong + a[i] * b[N-1-i];
    }
    return tong;
}
int main(){
    int t;cin>>t;
    while(t--){
        cin>>N;
        vector<long long> a;
		vector<long long> b;
        for(int i=0;i<N;i++){
            long long x;cin>>x;
            a.push_back(x);
        }
        for(int i=0;i<N;i++){
			long long x;cin>>x;
            b.push_back(x);
		}
        cout<<gtnn(a,b,N)<<endl;
    }
}