#include<bits/stdc++.h>
using namespace std;

long long minCost(long long arr[], long long n) {
    priority_queue<long long, vector<long long>, greater<long long> > pq(arr, arr + n);
    long long totalCost = 0;
    while (pq.size() > 1) {
        long long first = pq.top();
        pq.pop();
        long long second = pq.top();
        pq.pop();
        totalCost += first + second;
        pq.push(first + second);
    }
    return totalCost;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        long long n;
        cin >> n;
        long long arr[n];
        for (long long i = 0; i < n; i++) {
            cin >> arr[i];
        }
        cout << minCost(arr, n) << endl;
    }
    return 0;
}