#include<bits/stdc++.h>
using namespace std;

void BFS(int u, vector<int> adj[], vector<bool>& visited) {
    queue<int> q;
    q.push(u);
    visited[u] = true;
    while (!q.empty()) {
        int v = q.front();
        cout << v << " ";
        q.pop();
        for (int i = 0; i < adj[v].size(); i++) {
            if (!visited[adj[v][i]]) {
                q.push(adj[v][i]);
                visited[adj[v][i]] = true;
            }
        }
    }
}

int main() {
    int T;
    cin >> T;
    while (T--) {
        int V, E, u;
        cin >> V >> E >> u;
        vector<int> adj[V+1];
        vector<bool> visited(V+1, false);
        for (int i = 0; i < E; i++) {
            int u, v;
            cin >> u >> v;
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        BFS(u, adj, visited);
        cout << endl;
    }
    return 0;
}